# AntiBoost
This plugin logs multiple kills from the same player to another player to a text file to prevent boosting.
<br>No commands or permissions are required.

## Config
The only thing that needs to be changed in the config by the user is 
<pre>
kill-warning
</pre>
When a player (A) kills another player (B) x number of times in a day (the number in kill-warning), that information and a timestamp will be logged to boost-info.txt in the plugin folder.