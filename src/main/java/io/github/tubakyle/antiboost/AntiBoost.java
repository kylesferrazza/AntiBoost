package io.github.tubakyle.antiboost;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

import javax.xml.stream.events.Characters;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Kyle on 7/22/2014.
 * TODO: kills stay at 1 :(
 * TODO: test all features: kill-warning, kills increase, boost-info, date clearer, kill-warning persists
 */
public class AntiBoost extends JavaPlugin implements Listener {
    public void onEnable() {
        saveDefaultConfig();
        clearKillsIfDateChanged();
        saveResource("boost-info.txt", false);
        getServer().getPluginManager().registerEvents(this, this);
    }
    public void clearKillsIfDateChanged() {
        reloadConfig();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar = Calendar.getInstance();
        String currentDate = dateFormat.format(calendar.getTime());
        if (getConfig().isSet("last-date-used")) {
            if (!getConfig().getString("last-date-used").equals(currentDate)) {
                File cfg = new File(getDataFolder(), "config.yml");
                int killWarn = getConfig().getInt("kill-warning");
                cfg.delete();
                saveDefaultConfig();
                reloadConfig();
                getConfig().set("last-date-used", currentDate);
                getConfig().set("kill-warning", killWarn);
            }
        } else {
            getConfig().set("last-date-used", currentDate);
        }
        saveConfig();
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        reloadConfig();
        if (!(e.getEntity().getKiller() instanceof Player)) {
            return;
        }
        clearKillsIfDateChanged();
        Player p = e.getEntity();
        Player killer = p.getKiller();
        int kills;
        String configPath = p.getUniqueId().toString() + "." + killer.getUniqueId().toString();
        if (getConfig().isSet(configPath)) {
            kills = getConfig().getInt(configPath);
            kills += 1;
            getConfig().set(configPath, kills);
            saveConfig();
        } else {
            getConfig().set(configPath, 1);
            saveConfig();
            kills = 1;
        }
        configPath = p.getUniqueId().toString() + "." + killer.getUniqueId().toString();
        int killWarn = getConfig().getInt("kill-warning");
        if (getConfig().getInt(configPath) >= killWarn) {
            try {
                String path = getDataFolder() + File.separator + "boost-info.txt";
                FileWriter data = new FileWriter(path, true);
                DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM, yyyy 'at' h:mm a");
                Calendar calendar = Calendar.getInstance();
                String currentDate = dateFormat.format(calendar.getTime());
                String msg = "[" + currentDate + "] Player '" + killer.getDisplayName() + "' has killed player '" + p.getDisplayName() + "' " + kills + " times today.";
                data.append(msg);
                data.append(System.lineSeparator());
                data.flush();
                data.close();
            } catch (IOException e1) {
                getLogger().info("IOException in AntiBoost! Cannot save boost info.");
                e1.printStackTrace();
            }
        }
    }


}
